extern crate sdl2;

mod model;
mod display;
mod controller_gameover;
mod controller_pausing;
mod controller_playing;
mod resources;

use crate::model::State;
use crate::display::Display;
use sdl2::pixels::Color;
use sdl2::render::WindowCanvas;
use std::collections::HashMap;
use std::time::Duration;


fn main() {
    let sdl_context = sdl2::init().unwrap();

    let video = sdl_context.video().unwrap();
    let audio = sdl_context.audio().unwrap();
    let ttf = sdl2::ttf::init().unwrap();

    let window = video.window("snake", display::SCREEN_WIDTH, display::SCREEN_HEIGHT)
        .position_centered()
        .build()
        .unwrap();
    let canvas = window.into_canvas().build().unwrap();
    let texture_creator = canvas.texture_creator();

    let mut display = Display {
        video: &video,
        audio: &audio,
        ttf: &ttf,
        canvas,
        texture_creator: &texture_creator,
        sounds : HashMap::new(),
        fonts : HashMap::new(),
    };

    let resources = resources::Resources::build_resources(&texture_creator);

    display.initialize_audio_device(String::from("rattle"), &resources.sound_rattle);
    display.load_font(String::from("big"), String::from("/usr/share/fonts/truetype/ubuntu/Ubuntu-M.ttf"), 70);

    let mut model = model::build();

    draw_empty_frame(&mut display.canvas);
    display.canvas.present();

    let mut event_pump = sdl_context.event_pump().unwrap();

    while model.state != State::Quiting {
        draw_empty_frame(&mut display.canvas);

        match model.state {
            State::Playing => {
                controller_playing::display(&model, &resources, &mut display);
                controller_playing::input(&mut event_pump, &mut model);
                controller_playing::update(&mut model);
            },
            State::GameOver => {
                controller_gameover::display(&model, &resources, &mut display);
                controller_gameover::input(&mut event_pump, &mut model);
                controller_gameover::update(&mut model);
            },
            State::Pausing => {
                controller_pausing::display(&model, &resources, &mut display);
                controller_pausing::input(&mut event_pump, &mut model);
                controller_pausing::update(&mut model);
            },
            State::Quiting => {}
        }

        display.canvas.present();
        ::std::thread::sleep(Duration::from_millis(1000/60));
        model.tick += 1;
    }

}

fn draw_empty_frame(canvas : &mut WindowCanvas) {
    canvas.set_draw_color(Color::RGB(0x00, 0x00, 0x00));
    canvas.clear();
}
