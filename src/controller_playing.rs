use crate::display::Display;
use crate::display::TILE_SIZE;
use crate::display;
use crate::model::Direction;
use crate::model::Model;
use crate::model::State;
use crate::resources::Resources;

use rand::Rng;
use sdl2::EventPump;
use sdl2::event::Event;
use sdl2::keyboard::Keycode;
use sdl2::rect::Rect;
use sdl2::render::Texture;
use sdl2::render::WindowCanvas;

const GRID_WIDTH : u32 = display::SCREEN_WIDTH / display::TILE_SIZE;
const GRID_HEIGHT : u32 = display::SCREEN_HEIGHT / display::TILE_SIZE;

pub fn update(model : &mut Model) {
    if model.tick % (20 - model.speed as i128) == 0 {
        while model.next_direction.len() > 5 {
            model.next_direction.pop_front();
        }

        if !model.next_direction.is_empty() {
            let next_direction = model.next_direction.pop_front().unwrap();

            if next_direction != model.direction {
                if next_direction == Direction::Up && model.direction != Direction::Down {
                    model.direction = next_direction;
                } else if next_direction == Direction::Down && model.direction != Direction::Up {
                    model.direction = next_direction;
                } else if next_direction == Direction::Left && model.direction != Direction::Right {
                    model.direction = next_direction;
                } else if next_direction == Direction::Right && model.direction != Direction::Left {
                    model.direction = next_direction;
                }
            }
        }

        if !model.lengthen {
            model.snake.pop_front();
        } else {
            model.lengthen = false;
        }

        let mut new_head : (i32, i32) = (0, 0);
        let old_head = model.snake.get(model.snake.len() - 1).unwrap();
        new_head.0 += old_head.0;
        new_head.1 += old_head.1;

        match model.direction {
            Direction::Up => new_head.1 -= 1,
            Direction::Down => new_head.1 += 1,
            Direction::Left => new_head.0 -= 1,
            Direction::Right => new_head.0 += 1,
        }

        if new_head.0 >= GRID_WIDTH as i32 {
            new_head.0 = 0;
        } else if new_head.0 < 0 {
            new_head.0 = GRID_WIDTH as i32 - 1;
        }

        if new_head.1 >= GRID_HEIGHT as i32 {
            new_head.1 = 0;
        } else if new_head.1 < 0 {
            new_head.1 = GRID_HEIGHT as i32 - 1;
        }

        model.last_direction = model.direction;

        for coordinate in model.snake.iter() {
            if coordinate.0 == new_head.0 && coordinate.1 == new_head.1 {
                model.state = State::GameOver;
                break;
            }
        }

        model.snake.push_back(new_head);

        if new_head.0 == model.snack.0 && new_head.1 == model.snack.1 {
            model.lengthen = true;
            model.snack.0 = -1;
            model.snack.1 = -1;
            model.ticks_since_eaten = 0;
        }

        while model.snack.0 == -1 && model.snack.1 == -1 {
            model.snack.0 = rand::thread_rng().gen_range(0..GRID_WIDTH as i32);
            model.snack.1 = rand::thread_rng().gen_range(0..GRID_HEIGHT as i32);

            for coordinate in model.snake.iter() {
                if coordinate.0 == model.snack.0 && coordinate.1 == model.snack.1 {
                    model.snack.0 = -1;
                    model.snack.1 = -1;
                    break;
                }
            }
        }
    }

    model.ticks_since_eaten += 1;
}

pub fn display(model : &Model, resources : &Resources, display : &mut Display) {
    if model.snack.0 != -1 && model.snack.1 != -1 {
        draw_tile(&mut display.canvas, &resources.spritesheet, resources.spritesheet_info.snack.0, model.snack);
    }

    let mut include_tongue = false;
    if model.ticks_since_eaten < 40 {
        include_tongue = true;
    }

    for (index, coordinate) in model.snake.iter().rev().enumerate() {
        if index == 0 {
            draw_tile_directional(&mut display.canvas, &resources.spritesheet, resources.spritesheet_info.head, &model.direction, *coordinate);

            if include_tongue {
                let mut coordinate = (coordinate.0, coordinate.1);
                match model.direction {
                    Direction::Up => coordinate.1 -= 1,
                    Direction::Down => coordinate.1 += 1,
                    Direction::Left => coordinate.0 -= 1,
                    Direction::Right => coordinate.0 += 1,
                }

                draw_tile_directional(&mut display.canvas, &resources.spritesheet, resources.spritesheet_info.tongue, &model.direction, coordinate);
            }
        } else if index == model.snake.len() - 1 {
            draw_tile_directional(&mut display.canvas, &resources.spritesheet, resources.spritesheet_info.tail, &model.direction, *coordinate);

        } else {
            draw_tile_directional(&mut display.canvas, &resources.spritesheet, resources.spritesheet_info.body, &model.direction, *coordinate);
        }
    }

    if model.ticks_since_eaten == 1 {
        display.play_audio_device(String::from("rattle"), &resources.sound_rattle);
    }
}

pub fn input(event_pump : &mut EventPump, model : &mut Model) {
    let mut event = event_pump.poll_event();
    while event.is_some() {
        match event.unwrap() {
            Event::Quit {..} |
            Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                model.state = State::Quiting
            },
            Event::KeyDown { keycode: Some(Keycode::Space), .. } |
            Event::KeyDown { keycode: Some(Keycode::Return), .. } => {
                model.state = State::Pausing
            }
            Event::KeyDown { keycode: Some(Keycode::Up), .. } => {
                model.next_direction.push_back(Direction::Up);
            },
            Event::KeyDown { keycode: Some(Keycode::Down), .. } => {
                model.next_direction.push_back(Direction::Down);
            },
            Event::KeyDown { keycode: Some(Keycode::Left), .. } => {
                model.next_direction.push_back(Direction::Left);
            },
            Event::KeyDown { keycode: Some(Keycode::Right), .. } => {
                model.next_direction.push_back(Direction::Right);
            },
            _ => {}
        }
        event = event_pump.poll_event();
    }
}

fn draw_tile_directional(canvas : &mut WindowCanvas, spritesheet : &Texture, spritesheet_element : (Rect, Rect, Rect, Rect), direction : &Direction, coordinate : (i32, i32)) {
    let spritesheet_element_rect = match *direction {
        Direction::Down => spritesheet_element.2,
        Direction::Up => spritesheet_element.0,
        Direction::Left => spritesheet_element.3,
        Direction::Right => spritesheet_element.1,
    };

    draw_tile(canvas, &spritesheet, spritesheet_element_rect, coordinate);
}

fn draw_tile(canvas : &mut WindowCanvas, source_texture : &Texture, source_rect : Rect, target_coord : (i32, i32)) {
    let src = Rect::new(source_rect.x * (TILE_SIZE as i32), source_rect.y * (TILE_SIZE as i32), TILE_SIZE, TILE_SIZE);
    let dst = Rect::new(target_coord.0 * (TILE_SIZE as i32), target_coord.1 * (TILE_SIZE as i32), TILE_SIZE, TILE_SIZE);
    canvas.copy(&source_texture, src, dst).unwrap();
}
