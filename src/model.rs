use std::collections::VecDeque;

pub struct Model {
    pub snake : VecDeque<(i32, i32)>,
    pub direction : Direction,
    pub last_direction : Direction,
    pub next_direction : VecDeque<Direction>,
    pub tick : i128,
    pub state : State,
    pub snack : (i32, i32),
    pub lengthen : bool,
    pub speed : i32,
    pub ticks_since_eaten : i32,
    pub ticks_since_gameover : i32,
}

#[derive(PartialEq, Copy, Clone)]
pub enum Direction {
    Up,
    Down,
    Left,
    Right,
}

#[derive(PartialEq, Copy, Clone)]
pub enum State {
    Playing,
    Pausing,
    GameOver,
    Quiting,
}

pub fn build() -> Model {
    let mut model = Model {
        snake: VecDeque::with_capacity(1),
        direction: Direction::Down,
        last_direction: Direction::Down,
        next_direction: VecDeque::new(),
        tick: 0,
        state: State::Playing,
        snack: (-1, -1),
        lengthen: false,
        speed : 11,
        ticks_since_eaten : 1000,
        ticks_since_gameover : 0,
    };

    model.snake.push_back((5, 5));
    model.snake.push_back((5, 6));
    model.snake.push_back((5, 7));
    model.snake.push_back((5, 8));
    model.snake.push_back((5, 9));

    model
}
