use crate::display;

use sdl2::audio::AudioSpecWAV;
use sdl2::image::LoadTexture;
use sdl2::rect::Rect;
use sdl2::render::{Texture, TextureCreator};
use sdl2::video::WindowContext;

pub struct Resources <'a> {
    pub sound_rattle : AudioSpecWAV,
    pub spritesheet_info : SpritesheetInfo,
    pub spritesheet : Texture<'a>,
}

pub struct SpritesheetInfo {
    pub body : (Rect, Rect, Rect, Rect),
    pub head : (Rect, Rect, Rect, Rect),
    pub tongue : (Rect, Rect, Rect, Rect),
    pub tail : (Rect, Rect, Rect, Rect),
    pub snack : (Rect, Rect, Rect, Rect),
}

impl <'a> Resources<'a> {
    pub fn build_resources<'b>(texture_creator : &'a TextureCreator<WindowContext>) -> Self {
        Self {
            sound_rattle: AudioSpecWAV::load_wav("resources/rattle.wav").unwrap(),
            spritesheet_info: Resources::build_spritesheet_info(),
            spritesheet : texture_creator.load_texture("resources/snake.png").unwrap(),
        }
    }

    pub fn build_spritesheet_info() -> SpritesheetInfo {
        let t = display::TILE_SIZE;
        SpritesheetInfo {
            body   : (Rect::new(0, 0, t, t), Rect::new(1, 0, t, t), Rect::new(2, 0, t, t), Rect::new(3, 0, t, t)),
            head   : (Rect::new(0, 1, t, t), Rect::new(1, 1, t, t), Rect::new(2, 1, t, t), Rect::new(3, 1, t, t)),
            tongue : (Rect::new(0, 2, t, t), Rect::new(1, 2, t, t), Rect::new(2, 2, t, t), Rect::new(3, 2, t, t)),
            tail   : (Rect::new(0, 3, t, t), Rect::new(1, 3, t, t), Rect::new(2, 3, t, t), Rect::new(3, 3, t, t)),
            snack  : (Rect::new(0, 4, t, t), Rect::new(1, 4, t, t), Rect::new(2, 4, t, t), Rect::new(3, 4, t, t)),
        }
    }
}
