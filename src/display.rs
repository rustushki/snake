use std::collections::HashMap;

use sdl2::{audio::{AudioDevice, AudioSpecWAV, AudioSpecDesired, AudioCVT, AudioCallback}, AudioSubsystem, VideoSubsystem, render::{WindowCanvas, TextureCreator}, video::WindowContext, ttf::{Sdl2TtfContext, Font}};

pub const SCREEN_WIDTH : u32 = 800;
pub const SCREEN_HEIGHT : u32 = 600;
pub const TILE_SIZE : u32 = 25;

pub struct Display<'a, 'video, 'audio, 'ttf> {
    pub video : &'video VideoSubsystem,
    pub audio : &'audio AudioSubsystem,
    pub ttf : &'ttf Sdl2TtfContext,
    pub canvas : WindowCanvas,
    pub texture_creator : &'a TextureCreator<WindowContext>,
    pub sounds : HashMap<String, AudioDevice<SimpleCallback>>,
    pub fonts : HashMap<String, Font<'ttf, 'static>>,
}

impl <'a, 'video, 'audio, 'ttf> Display <'a, 'video, 'audio, 'ttf> {
    pub fn load_font(&mut self, name : String, path : String, size : u16) {
        self.fonts.insert(name, self.ttf.load_font(path, size).unwrap());
    }

    pub fn initialize_audio_device(&mut self, name : String, audio_spec : &AudioSpecWAV) {
        let desired_audio_spec = AudioSpecDesired {
            freq: Some(44_100),
            // Mono
            channels: Some(1),
            // Doesn't matter here, use the default value
            samples: None,
        };

        let audio_device = self.audio.open_playback(None, &desired_audio_spec, |spec| {
            let converter = AudioCVT::new(
                audio_spec.format,
                audio_spec.channels,
                audio_spec.freq,
                spec.format,
                spec.channels,
                spec.freq,
            )
            .unwrap();
            let data = converter.convert(audio_spec.buffer().to_vec());

            SimpleCallback {
                buffer: data,
                position: 0,
            }
        })
        .unwrap();

        self.sounds.insert(name, audio_device);
    }

    pub fn play_audio_device(&mut self, name : String, audio_spec : &AudioSpecWAV) {
        self.initialize_audio_device(name.clone(), audio_spec);
        self.sounds.get(&name).unwrap().resume();
    }
}

pub struct SimpleCallback {
    buffer: Vec<u8>,
    position: usize,
}

impl AudioCallback for SimpleCallback {
    type Channel = i16;

    // This function is called whenever the audio subsystem wants more data to play
    fn callback(&mut self, out: &mut [i16]) {
        for value in out.iter_mut() {
            *value = if self.position < self.buffer.len() {
                let sample = i16::from_le_bytes([
                    self.buffer[self.position],
                    self.buffer[self.position + 1],
                ]);
                self.position += 2;
                sample
            } else {
                0
            }
        }
    }
}

