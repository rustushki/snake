use sdl2::{EventPump, event::Event, keyboard::Keycode, pixels::Color};

use crate::{model::{Model, State}, resources::Resources, display::{Display, self}};

pub fn update(_model : &mut Model) {
}

pub fn input(event_pump : &mut EventPump, model : &mut Model) {
    let event = event_pump.poll_event();
    if event.is_some() {
        match event.unwrap() {
            Event::Quit {..} |
            Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                model.state = State::Quiting
            },
            Event::KeyDown { keycode: Some(Keycode::Space), .. } |
            Event::KeyDown { keycode: Some(Keycode::Return), .. } => {
                model.state = State::Playing
            }
            _ => {}
        }
    }
}

pub fn display(_model : &Model, _resources : &Resources, display : &mut Display) {
    display.canvas.set_draw_color(Color::RGB(0xAA, 0xAA, 0xAA));
    display.canvas.clear();

    let surface = display.fonts.get("big").unwrap().render("Paused").blended(Color::RGB(0x00, 0x00, 0x00)).unwrap();
    let src = surface.rect();
    let mut dst = surface.rect();
    dst.x = (display::SCREEN_WIDTH as i32 - src.w) / 2;
    dst.y = (display::SCREEN_HEIGHT as i32 - src.h) / 2;
    let texture = surface.as_texture(display.texture_creator).unwrap();

    display.canvas.copy(&texture, src, dst).unwrap();
}
